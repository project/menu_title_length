# Menu Title Length

- The Menu Title Length allows to change the length of the menu title field.

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

- This projects allows to set the length of the menu title,
  which by default is 255 characters.

- For a full description of the module, visit the project page:
  <https://www.drupal.org/project/menu_title_length/>

## Requirements

  This module requires no modules outside of Drupal core.

## Installation

- Install as you would normally install a contributed Drupal module. Visit:
  <https://www.drupal.org/documentation/install/modules-themes/modules-8/>
  for further information.

## Configuration

  If you want to change the size of menu title field, then
  go in configuration /admin/config/system/menu-title-length/settings
  and update.

  By default this value is 20.

## Maintainers

- Rajan Kumar - [rajan-kumar](https://www.drupal.org/u/rajan-kumar)
- Dipali Goel - [dipali.goel25](https://www.drupal.org/u/dipaligoel25)
