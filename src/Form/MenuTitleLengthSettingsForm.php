<?php

namespace Drupal\menu_title_length\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure menu_title_length settings for this site.
 */
class MenuTitleLengthSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'menu_title_length.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'menu_title_length_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['menu_title_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Menu Title Length'),
      '#default_value' => $config->get('menu_title_length'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->config(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('menu_title_length', $form_state->getValue('menu_title_length'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
